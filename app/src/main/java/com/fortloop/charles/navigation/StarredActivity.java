package com.fortloop.charles.navigation;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.support.v7.widget.PopupMenu;
import io.realm.Realm;
import io.realm.RealmResults;


public class StarredActivity extends AppCompatActivity {
    ListView lv;
    String[] books={"Nothing Here!"};
    String[] chapters={"Nothing Here!"};
    private static String[] titles;
    MAdapter adapter;
    public static boolean foundList=false;
    Snackbar sb;
    private Realm r;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starred);
        r = Realm.getInstance(StarredActivity.this);
        titles = getResources().getStringArray(R.array.books);
        lv = (ListView) findViewById(R.id.listview);
        getContent();
        adapter = new MAdapter(this, books, chapters);
        lv.setAdapter(adapter);
        listeners();

    }

    public void onDestroy(){
        super.onDestroy();
        r.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.starredmenu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            RealmResults<RealmBase> base = r.where(RealmBase.class).findAll();
            if(!base.isEmpty()) {
                for(int i = base.size() - 1; i >= 0; i--)
                    base.get(i).removeFromRealm();
                getContent();
                adapter = new MAdapter(StarredActivity.this, books, chapters);
                lv.setAdapter(adapter);
                sb.make(getCurrentFocus(), "Removed All!", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
            else
                sb.make(getCurrentFocus(), "Nothing to remove!", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void listeners(){
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(foundList) {
                    Intent intent=new Intent();
                    intent.putExtra("position",position);
                    setResult(2,intent);
                    finish();
                    }}
        });
        lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(foundList) {
                    final int a=position;
                    PopupMenu pm=new PopupMenu(StarredActivity.this,view);
                    pm.getMenuInflater().inflate(R.menu.menu_main,pm.getMenu());
                    pm.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            RealmResults<RealmBase> base = r.where(RealmBase.class).findAll();
                                        sb.make(getCurrentFocus(), "Removed from Starred "+base.size(), Snackbar.LENGTH_SHORT)
                                                .setAction("Action", null).show();
                                        base.get(a).removeFromRealm();
                                getContent();
                                adapter = new MAdapter(StarredActivity.this, books, chapters);
                                lv.setAdapter(adapter);
                            return true;
                        }
                    });
                    pm.setGravity(Gravity.END);
                    pm.show();
                }
                return true;
            }
        });
    }

    public void getContent(){
        String bookTitle="";
        String chapterT="";
        String[] CHAPTERNames;

        RealmResults<RealmBase> base = r.where(RealmBase.class).findAll();
        if (!base.isEmpty()){
            books=new String[base.size()];
            chapters=new String[base.size()];
            for(int i=0;i<base.size();i++)
            {
                if(base.get(i).getBook()==25)
                {bookTitle=titles[0];
                    CHAPTERNames=getResources().getStringArray(R.array.book1title);
                    chapterT=CHAPTERNames[base.get(i).getChapter()-1];
                }
                else if(base.get(i).getBook()==12)
                {bookTitle=titles[1];
                    CHAPTERNames=getResources().getStringArray(R.array.book2title);
                    chapterT=CHAPTERNames[base.get(i).getChapter()-1];
                }
                else if(base.get(i).getBook()==59)
                {bookTitle=titles[2];
                    CHAPTERNames=getResources().getStringArray(R.array.book3title);
                    chapterT=CHAPTERNames[base.get(i).getChapter()-1];
                }
                else if(base.get(i).getBook()==18)
                {bookTitle=titles[3];
                    CHAPTERNames=getResources().getStringArray(R.array.book4title);
                    chapterT=CHAPTERNames[base.get(i).getChapter()-1];
                }

                books[i]="Book : "+bookTitle;
                chapters[i]="Chapter "+base.get(i).getChapter()+": "+chapterT;
                foundList=true;
            }
        }
        else
        {
            books=new String[1];
            chapters=new String[1];
            books[0]="";
            chapters[0]="";
            foundList=false;
        }


    }
}


class MAdapter extends ArrayAdapter {
    String[] bookArray;
    String[] chapterArray;

    public MAdapter(Context context, String[] books1, String[] chapters1){
        super(context,R.layout.listview_row,R.id.listviewBook,books1);
        this.bookArray=books1;
        this.chapterArray=chapters1;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater=(LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row=inflater.inflate(R.layout.listview_row,parent,false);
        TextView bookTitle=(TextView) row.findViewById(R.id.listviewBook);
        TextView chapterTitle=(TextView) row.findViewById(R.id.listviewChapter);
        if(StarredActivity.foundList)
        {
            bookTitle.setText(position + 1+". " + chapterArray[position]);
            chapterTitle.setText("From "+ bookArray[position]);
        }
        else{
            bookTitle.setText("\n\nNothing Here!");
            chapterTitle.setText("");
        }
        return row;
    }

}
