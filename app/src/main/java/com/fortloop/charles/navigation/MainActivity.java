package com.fortloop.charles.navigation;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.support.design.widget.Snackbar;
import io.realm.Realm;
import io.realm.RealmResults;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static SectionsPagerAdapter mSectionsPagerAdapter;
    ViewPager mViewPager;
    public static int nchapter;
    public boolean saved;
    public static int nposition;
    private static String[] titles;
    private static String[] pinfo;
    private static BottomSheetBehavior b;
    FloatingActionButton fab;
    TextView t;
    static TextView book;
    Toolbar toolbar;
    Snackbar sb;
    DrawerLayout drawer;
    static ActionBarDrawerToggle toggle;
    NavigationView navigationView;
    TabLayout tabLayout;
    ToggleButton toggleButton;
    SharedPreferences sharedpref;
    String FileName="myfile";
    AlertDialog.Builder builder;
    AlertDialog alertDialog;
    TabLayout.Tab tab;
    SeekBar seekBar;
    private Realm mRealm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        int defaultnchapters=25;
        boolean defaultsaved=false;
        int defaultnposition=1;
        sharedpref=getSharedPreferences(FileName,Context.MODE_PRIVATE);
        nchapter=sharedpref.getInt("nchapters",defaultnchapters);
        nposition=sharedpref.getInt("position",defaultnposition);
        saved=sharedpref.getBoolean("saved",defaultsaved);
        if(!saved)
        {
            intro();
        }
        references();
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        b.setState(BottomSheetBehavior.STATE_HIDDEN);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        listeners();
        infodetails();
        infoVis();
        toggle();
        tab = tabLayout.getTabAt(nposition - 1);
        tab.select();
        mRealm.beginTransaction();
    }



    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode==2&&resultCode==2){
                    int position = data.getIntExtra("position", 0);
            RealmResults<RealmBase> base = mRealm.where(RealmBase.class).findAll();
                    nchapter = base.get(position).getBook();
                    nposition =base.get(position).getChapter();
                    reIntialiseTabs();
                    tab = tabLayout.getTabAt(base.get(position).getChapter() - 1);
                    tab.select();
        }
    }


    public void references(){
        mRealm = Realm.getInstance(MainActivity.this);
        seekBar=(SeekBar) findViewById(R.id.seekbar);
        toggleButton=(ToggleButton) findViewById(R.id.toggle);
        titles = getResources().getStringArray(R.array.books);
        t = (TextView) findViewById(R.id.sheetText);
        book = (TextView) findViewById(R.id.bookheader);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        b = BottomSheetBehavior.from(findViewById(R.id.bottomSheet));
        mViewPager = (ViewPager) findViewById(R.id.container);
    }


    public void onDestroy(){
        save();
        mRealm.commitTransaction();
        super.onDestroy();
        mRealm.close();
    }

    public void onStop(){
        save();
        super.onStop();
    }

    public void infodetails(){
    if(nchapter==25)
        pinfo = getResources().getStringArray(R.array.book1info);
    else if(nchapter==12)
        pinfo = getResources().getStringArray(R.array.book2info);
    else if(nchapter==59)
        pinfo = getResources().getStringArray(R.array.book3info);
    else if(nchapter==18)
        pinfo = getResources().getStringArray(R.array.book4info);
    seekBar.setMax(nchapter-1);
    }

    public void infoVis() {

        if (pinfo[nposition - 1].equals("0")) {
            fab.setVisibility(View.INVISIBLE);

        } else {
            fab.setVisibility(View.VISIBLE);
        }
    }

    public void toggle(){

        boolean found=false;
        RealmResults<RealmBase> base = mRealm.where(RealmBase.class).findAll();
        if(!base.isEmpty()) {
            for(int i = base.size() - 1; i >= 0; i--) {
                if(base.get(i).getBook()==nchapter&&base.get(i).getChapter()==nposition)
                {
                    toggleButton.setChecked(true);
                    found=true;
                    break;
                }
                else
                    found=false;
            }
            if(!found)
            {
                toggleButton.setChecked(false);
            }
        }
        else
        {
            toggleButton.setChecked(false);
        }

    }

    public void putToStar(){

        sb.make(getCurrentFocus(), "Added to Starred ", Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
        RealmBase rb = mRealm.createObject(RealmBase.class);
        rb.setBook(nchapter);
        rb.setChapter(nposition);

    }

    public void save(){
        sharedpref=getSharedPreferences(FileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedpref.edit();
        editor.putInt("nchapters",nchapter);
        editor.putInt("position",nposition);
        editor.putBoolean("saved",true);
        editor.commit();
    }

    public void intro(){
        builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Foreword :");
        builder.setMessage(getResources().getString(R.string.introduction));
        builder.setNegativeButton("Close",new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog,int id){
                alertDialog.cancel();
            }
        });
        alertDialog = builder.create();
        alertDialog.show();
        Resources r=getResources();
        float a= TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,300,r.getDisplayMetrics());
        float b= TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,450,r.getDisplayMetrics());
        alertDialog.getWindow().setLayout((int)a,(int)b);
    }

    public void reIntialiseTabs(){
            mViewPager.setAdapter(mSectionsPagerAdapter);
            infodetails();
            }

    public void listeners(){

        navigationView.setNavigationItemSelectedListener(this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                seekBar.setVisibility(View.INVISIBLE);
                fab.setVisibility(View.INVISIBLE);
                t.setText(pinfo[nposition-1]);
                b.setState(BottomSheetBehavior.STATE_EXPANDED);

            }
        });

        b.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if(newState==BottomSheetBehavior.STATE_EXPANDED){

                }
                else if(newState==BottomSheetBehavior.STATE_HIDDEN){
                    seekBar.setVisibility(View.VISIBLE);
                    fab.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            }
        });

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                if (isChecked) {
                    b.setState(BottomSheetBehavior.STATE_HIDDEN);
                    toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.goldstar));
                    boolean found = false;

                        RealmResults<RealmBase> base = mRealm.where(RealmBase.class).findAll();
                        if (!base.isEmpty()) {
                            for (int i = base.size() - 1; i >= 0; i--) {
                                if (base.get(i).getBook() == nchapter && base.get(i).getChapter() == nposition) {
                                    found = true;
                                    break;
                                } else
                                    found = false;
                            }
                            if (!found) {
                                putToStar();
                            }
                        } else {
                            putToStar();
                        }

                }
                else
                {
                    b.setState(BottomSheetBehavior.STATE_HIDDEN);
                    toggleButton.setBackgroundDrawable(ContextCompat.getDrawable(getApplicationContext(),R.drawable.greystar));

                    RealmResults<RealmBase> base = mRealm.where(RealmBase.class).findAll();
                    if(!base.isEmpty()) {
                        for(int i = base.size() - 1; i >= 0; i--) {
                            if(base.get(i).getBook()==nchapter&&base.get(i).getChapter()==nposition)
                            {
                                sb.make(getCurrentFocus(), "Removed from Starred ", Snackbar.LENGTH_SHORT)
                                        .setAction("Action", null).show();
                                base.get(i).removeFromRealm();
                            }

                        }
                    }
                }}
        });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                seekBar.setProgress(tab.getPosition());
                nposition = tab.getPosition();
                nposition=nposition+1;
                b.setState(BottomSheetBehavior.STATE_HIDDEN);
                infoVis();
                toggle();
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab){

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab){

            }
        });

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                tab = tabLayout.getTabAt(progress);
                tab.select();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search, menu);
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (id == R.id.book1) {
            nchapter = 25;
            reIntialiseTabs();
        } else if (id == R.id.book2) {
            nchapter = 12;
            reIntialiseTabs();
        } else if (id == R.id.book3) {
            nchapter = 59;
            reIntialiseTabs();
        } else if (id == R.id.book4) {
            nchapter = 18;
            reIntialiseTabs();
        } else if (id == R.id.about) {
            builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("About :");
            builder.setMessage(getResources().getString(R.string.about));
            builder.setNegativeButton("Close",new DialogInterface.OnClickListener(){
                @Override
                public void onClick(DialogInterface dialog,int id){
                    alertDialog.cancel();
                }
            });
            alertDialog = builder.create();
            alertDialog.show();
            }
        else if (id == R.id.starred) {
            Intent i=new Intent(getApplicationContext(),StarredActivity.class);
            startActivityForResult(i,2);
           }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public static class PlaceholderFragment extends Fragment {

        String[] CHAPTERNames;
        String[] CHAPTERDescription;
        LinearLayoutManager layoutManager;
        String[] description = new String[2];
        CardviewAdapter adapter;
        private static final String ARG_SECTION_NUMBER = "section_number";


        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
                final RecyclerView cardRecycler = (RecyclerView)inflater.inflate(
                        R.layout.recycler, container, false);

                    int position=getArguments().getInt(ARG_SECTION_NUMBER);
                    if(MainActivity.nchapter==25){book.setText(titles[0]); CHAPTERNames = getResources().getStringArray(R.array.book1title);
                        CHAPTERDescription = getResources().getStringArray(R.array.book1description);}
                    else if(MainActivity.nchapter==12){book.setText(titles[1]);CHAPTERNames = getResources().getStringArray(R.array.book2title);
                        CHAPTERDescription = getResources().getStringArray(R.array.book2description);}
                    else if(MainActivity.nchapter==59){book.setText(titles[2]);CHAPTERNames = getResources().getStringArray(R.array.book3title);
                        CHAPTERDescription = getResources().getStringArray(R.array.book3description);}
                    else if(MainActivity.nchapter==18){book.setText(titles[3]);CHAPTERNames = getResources().getStringArray(R.array.book4title);
                        CHAPTERDescription = getResources().getStringArray(R.array.book4description);}
                                        description[0]=CHAPTERNames[position-1];
                                        description[1]=CHAPTERDescription[position-1];
                                        adapter = new CardviewAdapter(description);
                                        cardRecycler.setAdapter(adapter);
                    layoutManager = new LinearLayoutManager(getActivity());
                    layoutManager.setOrientation(1);
               cardRecycler.setLayoutManager(layoutManager);
            return cardRecycler;
        }
    }





    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return nchapter;}

        @Override
        public CharSequence getPageTitle(int position) {
            int p=position+1;
            CharSequence a="CHAPTER "+p;
            return a;
        }
    }
}

