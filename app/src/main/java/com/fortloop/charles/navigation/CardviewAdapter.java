package com.fortloop.charles.navigation;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.support.v7.widget.CardView;

import static com.fortloop.charles.navigation.MainActivity.nchapter;


class CardviewAdapter extends RecyclerView.Adapter<CardviewAdapter.ViewHolder>{
    private String[] description;
    public CardviewAdapter(String[] description){
        this.description=description;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;
        public ViewHolder(CardView v) {
            super(v);
            cardView = v;
        }
    }

    @Override
    public CardviewAdapter.ViewHolder onCreateViewHolder(
            ViewGroup parent, int viewType){
        CardView cv = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview, parent, false);
        return new ViewHolder(cv);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){
        CardView cardView = holder.cardView;
        TextView tit = (TextView)cardView.findViewById(R.id.info_title);

        if(position==0){
            tit.setBackgroundColor(Color.parseColor("#FFFFFF"));
            tit.setTextColor(Color.parseColor("#000000"));
            tit.setGravity(Gravity.CENTER);
            cardView.setCardElevation(0);
            cardView.setCardBackgroundColor(Color.parseColor("#FFFFFF"));
        }
        tit.setText(description[position]);


    }
    @Override
    public int getItemCount(){
        return description.length;
    }
}