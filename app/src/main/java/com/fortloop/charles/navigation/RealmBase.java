package com.fortloop.charles.navigation;

import io.realm.RealmObject;


public class RealmBase extends RealmObject {


    private int book;
    private int chapter;

    public int getBook() {

        return book;
    }

    public int getChapter() {

        return chapter;
    }

    public void setBook(final int book) {
        this.book = book;
    }

    public void setChapter(final int chapter) {
        this.chapter = chapter;
    }
}
